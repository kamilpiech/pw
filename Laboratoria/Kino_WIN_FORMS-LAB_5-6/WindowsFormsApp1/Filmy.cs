﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Filmy
    {
        public string title { get; set; }
        public string age { get; set; }
        public string time { get; set; }
        public string production { get; set; }

        public Filmy(string title, string age, string time, string production)
        {
            this.title = title;
            this.age = age;
            this.time = time;
            this.production = production;
        }
    }
}
