﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1
{
    public class Klient
    {
        public string name { get; set; }
        public string surname { get; set; }
        public string email { get; set; }
        public int price { get; set; }
 
        public Klient(string name, string surname, string email, int price)
        {
            this.name = name;
            this.surname = surname;
            this.email = email;
            this.price = price;
        }

    }
}
