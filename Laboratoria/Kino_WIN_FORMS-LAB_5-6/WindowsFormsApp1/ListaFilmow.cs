﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class ListaFilmow : Form
    {
        public static List<Filmy> Films = Dodaj.Films;

        public ListaFilmow()
        {
            InitializeComponent();
        }

        private void ListaFilmow_Load(object sender, EventArgs e) //wyswietlenie filmow
        {
            listView1.Items.Clear();
            foreach (var filmy in Films)
            {
                var row = new ListViewItem(new String[] { filmy.title, filmy.age, filmy.time, filmy.production }); //zbior obiektow znajdujacych sie w klasie
                listView1.Items.Add(row);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
