﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Usun : Form
    {
        public static List<Filmy> Films = Dodaj.Films;
        public static string selectedFilm;

        public Usun()
        {
            InitializeComponent();
        }

        private void getData() //lista filmow
        {
            comboBox1.DataSource = Films;
            comboBox1.DisplayMember = "title";
            comboBox1.ValueMember = "title";
        }

        private void Usun_Load(object sender, EventArgs e)
        {
            getData();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click_1(object sender, EventArgs e) //usuwanie filmu
        {
            selectedFilm = comboBox1.GetItemText(comboBox1.SelectedItem);
            for (int i = 0; i < Films.Count; i++)
            {
                string name = (Films[i].title);
                if (name == selectedFilm)
                {
                    Films.RemoveAt(i);
                }
            }
            MessageBox.Show("Film pod tytułem '" + selectedFilm + "' został usunięty", "Usunięto", MessageBoxButtons.OK, MessageBoxIcon.Information);
            this.Close();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
