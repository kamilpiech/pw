﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp10
{
    public class Sprzedaze
    {
        public int id { get; set; }
        public string data { get; set; }
        public float kwota { get; set; }
        public string nazwa { get; set; }
        public string klient { get; set; }
        public string kontakt { get; set; }

        public Sprzedaze(int id, string nazwa, string data, string klient, string kontakt, float kwota)
        {
            this.id = id;
            this.data = data;
            this.kwota = kwota;
            this.nazwa = nazwa;
            this.klient = klient;
            this.kontakt = kontakt;
        }
    }
}
