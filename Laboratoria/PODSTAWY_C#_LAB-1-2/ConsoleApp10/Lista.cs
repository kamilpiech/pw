﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp10
{
    class Lista : IMetody
    {
        private List<Sprzedaze> tr;
        public Lista(List<Sprzedaze> tr)
        {
            this.tr = tr;
        }

        public void LiczbaRekordow()
        {
            Console.Clear();
            Console.WriteLine("Liczba rekordów w tabeli: " + tr.Count);
            Console.ReadKey();
        }

        public void WyczyscWszystko()
        {
            Console.Clear();
            tr.Clear();
        }

        public void WyczyscJeden()
        {
            Console.Clear();
            Console.WriteLine("Podaj id rekordu do usunięcia: ");
            string ra = Console.ReadLine();
            tr.RemoveAt(int.Parse(ra));
        }

        public void Wyswietl()
        {
            Console.Clear();
            Console.WriteLine("{0,-10}{1,-20} {2,-25} {3,-20} {4,-20} {5:N2}", "ID", "Nazwa produktu", "Data sprzedaży", "Klient", "Kontakt", "Kwota");
            Console.WriteLine();
            foreach (Sprzedaze t in tr)
            { 
                Console.WriteLine("{0,-10}{1,-20} {2,-25} {3,-20} {4,-20} {5:N2}", t.id, t.nazwa, t.data, t.klient, t.kontakt, t.kwota);
            }
            Console.ReadKey();
        }

        public void Dodaj(Sprzedaze t1)
        {
            Console.Clear();
            Console.WriteLine("Podaj nazwę sprzedanego przemiotu lub usługi: ");
            string nz = Console.ReadLine();
            t1.nazwa = nz;
            DateTime data = DateTime.UtcNow.ToLocalTime();
            t1.data = (data.ToString("dd-MM-yyyy hh:mm:ss"));
            Console.WriteLine("Podaj nazwe kupującego: ");
            string kl = Console.ReadLine();
            t1.klient = kl;
            Console.WriteLine("Podaj jego nr. telefonu lub adres e-mail: ");
            string kt = Console.ReadLine();
            t1.kontakt = kt;
            Console.WriteLine("Podaj kwote transakcji: ");
            string kw = Console.ReadLine();
            t1.kwota = float.Parse(kw);
            tr.Add(new Sprzedaze(t1.id, t1.nazwa, t1.data, t1.klient, t1.kontakt, t1.kwota));
            t1.id++;
        }

        public void SumaT()
        {
            Console.Clear();
            float suma = 0;
            foreach (Sprzedaze t in tr)
            {
                suma += t.kwota;
            }
            Console.WriteLine("Suma kwot sprzedaży " + String.Format("{0:N2}", suma));
            Console.ReadKey();
        }

        public void SredniaT()
        {
            Console.Clear();
            float srednia = 0;
            foreach (Sprzedaze t in tr)
            {
                srednia += t.kwota;
            }
            srednia = srednia / tr.Count;
            Console.WriteLine("Średnia kwota sprzedaży " + String.Format("{0:N2}", srednia));
            Console.ReadKey();
        }
    }
}
