﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp10
{
    class Menu
    {
        private Sprzedaze t1;
        public Menu()
        {
        }

        Menu(Sprzedaze t1)
        {
            this.t1 = t1;
        }

        public void Menu1(Sprzedaze t1)
        {
            List<Sprzedaze> tr = new List<Sprzedaze>();
            Lista ls = new Lista(tr);
            while (true)
            {
                Console.Clear();
                Console.WriteLine(">>> Sprzedaże <<<");
                Console.WriteLine("> 1 - wprowadź dane ");
                Console.WriteLine("> 2 - wyświetl dane ");
                Console.WriteLine("> 3 - wyświetli liczbe rekordów");
                Console.WriteLine("> 4 - usuń wybrany rekord");
                Console.WriteLine("> 5 - usuń wszystkie rekordy");
                Console.WriteLine("> 6 - suma kwot sprzedaży");
                Console.WriteLine("> 7 - średnia kwot sprzedaży");
                Console.WriteLine("> 8 - koniec programu");
                ConsoleKeyInfo klawisz = Console.ReadKey();
                switch (klawisz.Key)
                {
                    case ConsoleKey.D1:
                        ls.Dodaj(t1);
                        break;
                    case ConsoleKey.D2:
                        ls.Wyswietl();
                        break;
                    case ConsoleKey.D3:
                        ls.LiczbaRekordow();
                        break;
                    case ConsoleKey.D4:
                        ls.WyczyscJeden();
                        break;
                    case ConsoleKey.D5:
                        ls.WyczyscWszystko();
                        break;
                    case ConsoleKey.D6:
                        ls.SumaT();
                        break;
                    case ConsoleKey.D7:
                        ls.SredniaT();
                        break;
                    case ConsoleKey.D8:
                        Environment.Exit(0); break;
                    default: break;
                }
            }
        }
    }
}
