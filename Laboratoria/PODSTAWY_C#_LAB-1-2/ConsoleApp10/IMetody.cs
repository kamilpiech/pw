﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp10
{
    public interface IMetody
    {
        void LiczbaRekordow();
        void WyczyscWszystko();
        void WyczyscJeden();
        void Wyswietl();
        void Dodaj(Sprzedaze t1);
        void SumaT();
        void SredniaT();
    }
}
