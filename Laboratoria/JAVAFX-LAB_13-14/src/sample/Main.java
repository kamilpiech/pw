package sample;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

public class Main extends Application {
    public static void main(String[] args) { launch(args); }

    @Override public void start(Stage stage) throws Exception {
        WebView webview = new WebView();
        webview.getEngine().load(
                "http://www.youtube.com/embed/y1LDmeNPZgA?autoplay=1"
        );
        webview.setPrefSize(1800, 900);

        stage.setScene(new Scene(webview));
        stage.setTitle("Kamil Piech 38586");
        stage.show();
    }
}